
/**
 * tokenize a text string
 * @param {string} input - The text string to tokenize  
 * @returns {Array} - the list of tokens
 */
module.exports = function (input) {
    //TODO: investigate just exactly how naive this
    //      implementation is.  Because I'm guessing
    //      it's probably babytown frolics
    const re = /\S+/g;
    return input.match(re);
}
