
const descriptions = {
    "0": "no chance",
    "10": "a super slim chance",
    "20": "a very slim chance",
    "30": "a slim chance",
    "40": "a decent chance",
    "50": "a 50-50 chance",
    "60": "a better than average chance",
    "70": "a good chance",
    "80": "a really good chance",
    "90": "an almost definite chance",
    "100": "a 100% chance"
};


class ParserResult {
    constructor(name, percentage) {
        this.testName = name || 'default';
        this.percentage = percentage || 0;
        this.resultDescription = getDescription(percentage);
    }

    getDescription(percentage) {
        return descriptions[percentage.toString()];
    }
}
