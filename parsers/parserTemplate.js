/**
 *  template for a new parser.  Uh.  Don't actually USE this one, just
 *      copy it to make new ones.
 */

const ParserResult = require('../lib/ParserResult');

module.exports = function(tokens) {

    // do your parsing here, then:

    return new ParserResult('contains a phone number', 0);
}